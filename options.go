package messaging

import (
	"errors"

	"github.com/nats-io/nats.go"
)

// options are the options that can be set for a messaging connector.
// For more info see SetOptions
type options struct {
	requiredStreams            []nats.StreamConfig
	requiredConsumers          map[string][]nats.ConsumerConfig
	additionalNatsOptions      []nats.Option
	additionalJetStreamOptions []nats.JSOpt
	encoder                    nats.Encoder
}

// Option is one option that can be set for a messaging connector
type Option interface {
	setOpt(opts *options) error
}

// optionFn is a function setting one option on the options struct
type optionFn func(opts *options) error

// setOpt is the function called by SetOptions to actually set an option in the options struct
func (opt optionFn) setOpt(opts *options) error {
	return opt(opts)
}

// SetOptions sets the given options on the messaging connector.
//
// Options are ment to be set by the calling application.
// For configuration supplied during deployment see Config.
//
// Options set will be overwritten, not merged.
//
// This method must be called becore calling Connect.
// Returns an error if the messaging connector is already connected.
//
// Example setting some options:
// mc := NewMessagingConnector(...)
// mc.SetOptions(WithRequiredStreams(...), WithAdditionalNatsOptions(...), ...)
//
func (mc *MsgCon) SetOptions(opts ...Option) error {
	if mc.Connected() {
		return errors.New("Cannot set options on a messaging connector that is already connected.")
	}

	for _, opt := range opts {
		if err := opt.setOpt(&mc.options); err != nil {
			return err
		}
	}

	return nil
}

// WithRequiredStreams returns the corresponding Option
// indicating that streams with the given config should be configured
// during Connect
//
// Streams are part of JetStream, the protocol on top of NATS with durability gurantees.
// Streams should be properly configured when using one of the durable publish or subscribe functions.
func WithRequiredStreams(requiredStreams ...nats.StreamConfig) Option {
	return optionFn(func(ops *options) error {
		ops.requiredStreams = requiredStreams
		return nil
	})
}

// WithRequiredConsumers returns the corresponding Option
// indicating that consumers with the given config should be configured for the given stream
// during Connect
//
// Consumers are part of JetStream, the protocol on top of NATS with durability gurantees.
// Streams should be properly configured when using one of the durable subscribe functions.
//
// This will only overwrite the list of required consumers for the given stream name
func WithRequiredConsumers(streamName string, requiredConsumers ...nats.ConsumerConfig) Option {
	return optionFn(func(ops *options) error {
		ops.requiredConsumers[streamName] = requiredConsumers
		return nil
	})
}

// WithAdditionalNatsOptions returns the corresponding Option
// for passing the given additional options to the nats.Connect call
func WithAdditionalNatsOptions(additionalNatsOptions ...nats.Option) Option {
	return optionFn(func(ops *options) error {
		ops.additionalNatsOptions = additionalNatsOptions
		return nil
	})
}

// WithAdditionalJetStreamOptions returns the corresponding Option
// for passing the given additional options the the nats.Conn.JetStream call
func WithAdditionalJetStreamOptions(additionalJetStreamOptions ...nats.JSOpt) Option {
	return optionFn(func(ops *options) error {
		ops.additionalJetStreamOptions = additionalJetStreamOptions
		return nil
	})
}

// WithEncoder returns the corresponding Option
// for using the given encoder for encoding payloads
func WithEncoder(encoder nats.Encoder) Option {
	return optionFn(func(ops *options) error {
		ops.encoder = encoder
		return nil
	})
}
