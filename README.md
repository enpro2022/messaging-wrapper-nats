# messaging-wrapper-nats

Wrapper library around the messaging technology NATS

This is a very thin wrapper around <https://github.com/nats-io/nats.go>, mainly meant to:

* provide sensible default options for our project
* help with creating the connection to the messaging infrastructure
* Add encoding/decoding support for the payload

This library does not abstract away from the NATS specifics.
It also allows to access the underlying upstream contexts to allow
for advanced usage of the underlying library without adding explicit support here.

This library supports the JetStream protocol, which provides reliability on top of NATS and pure NATS.
In most cases durable messages (e.g. JetStream) should be preferred.
However, JetStream requires some application-specific setting up of Streams and Consumers.
TODO: Explain how to determine correct Streams and Consumers setup.

For Request-Reply style communication pure NATS must be used.
Reliability can be archived by retrying the request on failure.


## Example usage

Publishing:

```go
type complexData struct {
    name string
    whatever int
    ...
}

func publish() error {
    messagingConfig := ...
    mc := NewMessagingConnector(messagingConfig)
    if err := mc.Connect(); err != nil {
        return err
    }
    _, err := mc.PublishDurable("subject1", "payload1") //payload1 is a string
    if err != nil {
        return err
    }

    _, err :=  mc.PublishDurable("subject2", 123) //payload2 is a number
    if err != nil {
        return err
    }

    _, err := mc.PublishDurable("subject3", &complexData{
        name: "asd",
        whatever: 5,
        ...
    }) //payload3 is a dict

    if err != nil {
        return err
    }
}
```

Subscribing:

```go
type complexData struct {
    name string
    whatever int
    ...
}

func subscribe() {
    messagingConfig := ...
    mc := NewMessagingConnector(messagingConfig)
    if err := mc.Connect(); err != nil {
        return err
    }

    sub, err := mc.SubscribeDurableSync("Sample-Stream", "Sample-Consumer")
    if err != nil {
        return err
    }
    msg, err := sub.NextMsg(timeout)
    if err != nil {
        return err
    }
    if err := sub.Unsubscribe(); err != nil {
        return err
    }
    var payload *string // payload1 is a string
    if err := mc.Encoder().Decode(msg.Subject, msg.Data, payload); err != nil {
        return err
    }

    sub, err := mc.SubscribeDurableAsync("Sample-Stream", "Sample-Consumer", func(m *nats.Msg) {
        var payload *complexData // payload3 is a dict
        if err := mc.Encoder().Decode(msg.Subject, msg.Data, payload); err != nil {
            //somehow handle
        }
    })
}
```
