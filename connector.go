package messaging

import (
	"github.com/nats-io/nats.go"
)

// MsgCon is a messaging connector able to connect to the messaging infrastructure
type MsgCon struct {
	config       Config
	options      options
	connected    bool
	disconnected bool

	natsContext      *nats.Conn
	jetStreamContext *nats.JetStreamContext
}

// NewMessagingConnector creates a new messaging connector with the given config
//
// It does not yet actually connect to the messaging infrastructure.
// Use Connect for that.
func NewMessagingConnector(config Config) MsgCon {
	return MsgCon{
		config: config,
		options: options{
			requiredStreams:            []nats.StreamConfig{},
			requiredConsumers:          map[string][]nats.ConsumerConfig{},
			additionalNatsOptions:      []nats.Option{},
			additionalJetStreamOptions: []nats.JSOpt{},
			encoder:                    nats.EncoderForType("json"),
		},
		connected:    false,
		disconnected: false,
	}
}

func (mc *MsgCon) Config() Config {
	return mc.config
}

func (mc *MsgCon) Options() options {
	return mc.options
}

func (mc *MsgCon) Connected() bool {
	return mc.connected
}

func (mc *MsgCon) Disconnected() bool {
	return mc.connected
}

func (mc *MsgCon) NatsContext() *nats.Conn {
	return mc.natsContext
}

func (mc *MsgCon) JetStreamContext() *nats.JetStreamContext {
	return mc.jetStreamContext
}

func (mc *MsgCon) Encoder() nats.Encoder {
	return mc.Options().encoder
}
