package messaging

import (
	"errors"
	"fmt"

	"github.com/nats-io/nats.go"
)

func (mc *MsgCon) configureRequiredStreams() error {
	var missingStreams = map[string]*nats.StreamConfig{}

	for idx, stream := range mc.Options().requiredStreams {
		missingStreams[stream.Name] = &mc.Options().requiredStreams[idx]
	}

	for name := range (*mc.JetStreamContext()).StreamNames() {
		delete(missingStreams, name)
	}

	for name, stream := range missingStreams {
		if mc.Config().RequiredStreamsConfigurationAction == DoNotTouch {
			return errors.New(fmt.Sprintf("Required jetStream stream %s is missing, but RequiredStreamsConfigurationAction is DoNotTouch.", name))
		} else {
			_, err := (*mc.JetStreamContext()).AddStream(stream)
			if err != nil {
				return err
			}
		}
	}

	for _, stream := range mc.Options().requiredStreams {
		if mc.Config().RequiredStreamsConfigurationAction == AlwaysUpdate {
			_, err := (*mc.JetStreamContext()).UpdateStream(&stream)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (mc *MsgCon) configureRequiredConsumers() error {
	for streamName, requiredConsumers := range mc.Options().requiredConsumers {
		var missingConsumers = map[string]*nats.ConsumerConfig{}

		for idx, cons := range requiredConsumers {
			missingConsumers[cons.Durable] = &requiredConsumers[idx]
		}

		for name := range (*mc.JetStreamContext()).ConsumerNames(streamName) {
			delete(missingConsumers, name)
		}

		for name, cons := range missingConsumers {
			if mc.Config().RequiredConsumersConfigurationAction == DoNotTouch {
				return errors.New(fmt.Sprintf("Required jetStream consumer %s for stream %s is missing, but RequiredConsumersConfigurationAction is DoNotTouch.", name, streamName))
			} else {
				_, err := (*mc.JetStreamContext()).AddConsumer(streamName, cons)
				if err != nil {
					return err
				}
			}
		}

		for _, cons := range requiredConsumers {
			if mc.Config().RequiredConsumersConfigurationAction == AlwaysUpdate {
				_, err := (*mc.JetStreamContext()).UpdateConsumer(streamName, &cons)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}

// Connect actually connects this messaging connector instance to the messaging infrastructure.
//
// To customize the connection please call SetOptions.
//
// This message can only be called once per messaging connector.
// Returns an error when called a second time.
func (mc *MsgCon) Connect() error {
	if mc.Connected() {
		return errors.New("Cannot connect a MessagingConnection that is already connected.")
	}
	nc, err := nats.Connect(mc.config.ServerURL, mc.Options().additionalNatsOptions...)
	if err != nil {
		return err
	}
	mc.natsContext = nc
	js, err := nc.JetStream(mc.Options().additionalJetStreamOptions...)
	if err != nil {
		return err
	}
	mc.jetStreamContext = &js

	if err := mc.configureRequiredStreams(); err != nil {
		return err
	}

	if err := mc.configureRequiredConsumers(); err != nil {
		return err
	}

	mc.connected = true
	return nil
}

// Disconnect this messaging connector.
// After this is called, there is no way to reconnect with this messaging connector.
func (mc *MsgCon) Disconnect() error {
	mc.NatsContext().Close()
	mc.connected = false
	mc.disconnected = true
	return nil
}
