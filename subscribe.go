package messaging

import (
	"github.com/nats-io/nats.go"
)

// DefaultDurableSubOptions returns some sensible default SubOpts for the JetStream Sublish Functions
func (mc MsgCon) DefaultDurableSubOptions() []nats.SubOpt {
	return []nats.SubOpt{}
}

// SubscribeSync subscribes to a subject, retrieving messages synchronously.
//
// To decode the payload use MsgCon.Encoder.Decode
//
// This method does not use JetStream, therefore no QOS is guaranteed.
// For reliable messaging see SubscribeDurableSync
//
// For more information on the behavior, parameters, and return value see nats.Conn.SubscribeSync
func (mc MsgCon) SubscribeSync(subject string) (*nats.Subscription, error) {
	return mc.NatsContext().SubscribeSync(subject)
}

// SubscribeAsync subscribes to a subject, retrieving messages asnchronously.
//
// To decode the payload use MsgCon.Encoder.Decode
//
// This method does not use JetStream, therefore no QOS is guaranteed.
// For reliable messaging see SubscribeDurableAsync
//
// For more information on the behavior, parameters, and return value see nats.Conn.Subscribe
func (mc MsgCon) SubscribeAsync(subject string, handler nats.MsgHandler) (*nats.Subscription, error) {
	return mc.NatsContext().Subscribe(subject, handler)
}

// SubscribeDurableSync durably subscribes to a subject, retrieving messages synchronously.
// It uses the preconfigured consumer with the given name on the stream with the given name.
// The subject is defined by the configuration of the stream.
//
// To decode the payload use MsgCon.Encoder.Decode
//
// This method does use JetStream and the messages are therefore durable.
// However you must make sure the Streams and Consumers are setup correctly.
//
// For more information on the behavior, parameters, and return value see nats.JetStreamContext.SubscribeSync
func (mc MsgCon) SubscribeDurableSync(streamName string, consumerName string) (*nats.Subscription, error) {
	var subOpts = append(mc.DefaultDurableSubOptions(), nats.Bind(streamName, consumerName))
	return (*mc.JetStreamContext()).SubscribeSync("", subOpts...)
}

// SubscribeDurableAsync durably subscribes to a subject, retrieving messages asnchronously.
// It uses the preconfigured consumer with the given name on the stream with the given name.
// The subject is defined by the configuration of the stream.
//
// To decode the payload use MsgCon.Encoder.Decode
//
// This method does use JetStream and the messages are therefore durable.
// However you must make sure the Streams and Consumers are setup correctly.
//
// For more information on the behavior, parameters, and return value see nats.JetStreamContext.Subscribe
func (mc MsgCon) SubscribeDurableAsync(streamName string, consumerName string, handler nats.MsgHandler) (*nats.Subscription, error) {
	var subOpts = append(mc.DefaultDurableSubOptions(), nats.Bind(streamName, consumerName))
	return (*mc.JetStreamContext()).Subscribe("", handler, subOpts...)
}
