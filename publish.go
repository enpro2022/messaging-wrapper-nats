package messaging

import (
	"time"

	"github.com/nats-io/nats.go"
)

// DefaultDurablePubOptions returns some sensible default PubOpts for the JetStream Publish Functions
func (mc MsgCon) DefaultDurablePubOptions() []nats.PubOpt {
	return []nats.PubOpt{}
}

// Publish publishes a message with the given payload on the given subject.
//
// The payload may be anything that can be encoded with the selected encoder.
//
// This method does not use JetStream, therefore no QOS is guaranteed.
// For reliable messaging see PublishDurable
//
// Returns an error when encoding fails
//
// For more information on the behavior, parameters, and return value see nats.Conn.Publish
func (mc MsgCon) Publish(subject string, payload interface{}) error {
	bytes, err := mc.Encoder().Encode(subject, payload)
	if err != nil {
		return err
	}
	return mc.NatsContext().Publish(subject, bytes)
}

// Request will do a request with the given payload to the given subject and wait the given amount of time for the response.
//
// The payload may be anything that can be encoded with the selected encoder.
//
// This method does not use JetStream, therefore no QOS is guaranteed.
// Request-reply is not supported by JetStream.
// Reliability in request-reply can be achived by retrying the request on failure.
//
// Returns an error when encoding fails
//
// For more information on the behavior, parameters, and return value see nats.Conn.Request
func (mc MsgCon) Request(subject string, payload interface{}, timeout time.Duration) (*nats.Msg, error) {
	bytes, err := mc.Encoder().Encode(subject, payload)
	if err != nil {
		return nil, err
	}
	return mc.NatsContext().Request(subject, bytes, timeout)
}

// Publish publishes a message with the given payload on the given subject.
//
// The payload may be anything that can be encoded with the selected encoder.
//
// This method does use JetStream and the messages are therefore durable.
// However you must make sure the Streams and Consumers are setup correctly.
//
// Returns an error when encoding fails
//
// For more information on the behavior, parameters, and return value see nats.JetStreamContext.Publish
func (mc MsgCon) PublishDurable(subject string, payload interface{}) (*nats.PubAck, error) {
	bytes, err := mc.Encoder().Encode(subject, payload)
	if err != nil {
		return nil, err
	}
	return (*mc.JetStreamContext()).Publish(subject, bytes, mc.DefaultDurablePubOptions()...)
}
