package messaging

import (
	"errors"
	"fmt"
)

// PersistentConfigurationAction is a type indictaing what to do with configuration options persisted in the nats infrastructure
type PersistentConfigurationAction string

const (
	// Do not configure instances of this option ever
	DoNotTouch PersistentConfigurationAction = "DoNotTouch"
	// Create required instances of this option when they are not present but do not update existing instances
	CreateIfMissing PersistentConfigurationAction = "CreateIfMissing"
	// Always update the configuration of the required instances of this option
	AlwaysUpdate PersistentConfigurationAction = "AlwaysUpdate"
)

func (a PersistentConfigurationAction) IsValid() bool {
	switch a {
	case DoNotTouch, CreateIfMissing, AlwaysUpdate:
		return true
	}
	return false
}

func (action *PersistentConfigurationAction) Decode(value string) error {
	*action = PersistentConfigurationAction(value)
	if action.IsValid() {
		return nil
	}
	return errors.New(fmt.Sprintf("Invalid value for type PersistentConfigurationAction: %s", value))
}

// Config is the configuration used by a messaging connector.
// These are intended to be set by the ops people deploying the application.
// For options set by the calling application see Options.
// It is made to be processed by https://github.com/kelseyhightower/envconfig but could also be filled manually
type Config struct {
	ServerURL                            string                        `default:"nats://127.0.0.1:4222" split_words:"true" desc:"The url of the NATS server"`
	RequiredStreamsConfigurationAction   PersistentConfigurationAction `default:"CreateIfMissing" split_words:"true" desc:"What to do with for the required jetStream streams (DoNotTouch, CreateIfMissing, AlwaysUpdate)."`
	RequiredConsumersConfigurationAction PersistentConfigurationAction `default:"CreateIfMissing" split_words:"true" desc:"What to do with for the required jetStream consumers (DoNotTouch, CreateIfMissing, AlwaysUpdate)."`
}
